package com.cezimbra.projeto.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cezimbra on 06/09/2016.
 */
public class BackendResponse {

    @SerializedName("data")
    @Expose
    private List<Contact> data = new ArrayList<>();

    /**
     *
     * @return
     * The data
     */
    public List<Contact> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<Contact> data) {
        this.data = data;
    }
}
