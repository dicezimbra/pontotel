package com.cezimbra.projeto.interfaces;

import com.cezimbra.projeto.model.BackendResponse;
import com.cezimbra.projeto.model.Contact;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by cezimbra on 31/08/2016.
 */

public interface BackendInterface {
    @GET("pontotel-docs/data.json")
    Call<BackendResponse> getContacts();
}
