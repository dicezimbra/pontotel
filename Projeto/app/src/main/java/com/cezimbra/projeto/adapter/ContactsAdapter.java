package com.cezimbra.projeto.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.cezimbra.projeto.R;
import com.cezimbra.projeto.model.Contact;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by cezimbra on 31/08/2016.
 */

public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<Contact> mContacts;


    public ContactsAdapter(Context context, ArrayList<Contact> contacts) {
        this.mContext = context;
        this.mContacts = contacts;
    }

    private void bindDefaultFeedItem(int position,final ContactHolder holder) {

        try {

            final Contact contact = mContacts.get(position);
            holder.name.setText(contact.getName());
            holder.pwd.setText(contact.getPwd());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_contact, parent, false);
        final ContactHolder cellFeedViewHolder = new ContactHolder(view);
        return cellFeedViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        bindDefaultFeedItem(position, (ContactHolder) viewHolder);
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    public static class ContactHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.name)
        TextView name;

        @InjectView(R.id.pwd)
        TextView pwd;

        @InjectView(R.id.click)
        View click;

        public ContactHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
