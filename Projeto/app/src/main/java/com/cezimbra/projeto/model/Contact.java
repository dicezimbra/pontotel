
package com.cezimbra.projeto.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Contact extends RealmObject implements Serializable {


    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("pwd")
    @Expose
    private String pwd;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }


    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public enum CONTACTS {
        ID,
        NAME,
        PWD
    }

    @Override
    public String toString() {

        JSONObject obj = new JSONObject();
        try {

            obj.put(String.valueOf(CONTACTS.ID), String.valueOf(this.id));
            obj.put(String.valueOf(CONTACTS.NAME), this.name);
            obj.put(String.valueOf(CONTACTS.PWD), this.pwd);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj.toString();
    }

    public static Contact toObject(String sContact) {

        Contact contact = new Contact();
        try {
            JSONObject obj = new JSONObject(sContact);
            contact.setId(obj.getString(String.valueOf(CONTACTS.ID)));
            contact.setName(obj.getString(String.valueOf(CONTACTS.NAME)));
            contact.setPwd(obj.getString(String.valueOf(CONTACTS.PWD)));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contact;
    }

}
