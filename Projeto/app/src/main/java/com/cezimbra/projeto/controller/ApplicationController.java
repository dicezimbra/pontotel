package com.cezimbra.projeto.controller;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by cezimbra on 31/08/2016.
 */
public class ApplicationController extends Application {

    static ApplicationComponent mApplicationComponent;
    static public String IP = "https://s3-sa-east-1.amazonaws.com/";
    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this, IP))
                .build();
    }
    public static ApplicationController from(@NonNull Context context) {
        return (ApplicationController) context.getApplicationContext();
    }

    public static ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

}
