package com.cezimbra.projeto.interfaces;

/**
 * Created by cezimbra on 31/08/2016.
 */
public interface RealmChangeInterface {
    public void onRealmChanges();
}
