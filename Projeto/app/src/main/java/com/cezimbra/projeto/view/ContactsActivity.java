package com.cezimbra.projeto.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cezimbra.projeto.R;
import com.cezimbra.projeto.adapter.ContactsAdapter;
import com.cezimbra.projeto.controller.ApplicationComponent;
import com.cezimbra.projeto.interfaces.BackendInterface;
import com.cezimbra.projeto.interfaces.RealmChangeInterface;
import com.cezimbra.projeto.model.BackendResponse;
import com.cezimbra.projeto.model.Contact;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cezimbra on 31/08/2016.
 */
public class ContactsActivity extends BaseActivity implements Callback<BackendResponse> {


    @Inject
    BackendInterface mBackend;

    @InjectView(R.id.rvList)
    RecyclerView mRecyclerView;

    private RecyclerView.LayoutManager mLayoutManager;

    private RecyclerView.Adapter mAdapter;

    ArrayList<Contact> mContactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        setupToolbar();
        injectViews();

        mContactList = new ArrayList<>();

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ContactsAdapter(this, mContactList);
        mRecyclerView.setAdapter(mAdapter);

        Call<BackendResponse> call = mBackend.getContacts();
        call.enqueue(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onResponse(Call<BackendResponse> call, Response<BackendResponse> response) {

        mContactList.clear();

        for (int i = 0; i < response.body().getData().size(); i++) {
            Contact contact = response.body().getData().get(i);
            mContactList.add(contact);
        }

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onFailure(Call<BackendResponse> call, Throwable t) {
        t.printStackTrace();
    }

    @Override
    protected void performInjection(ApplicationComponent component) {
        component.inject(this);
    }
}
