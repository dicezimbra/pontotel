package com.cezimbra.projeto.controller;

import com.cezimbra.projeto.view.ContactsActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by cezimbra on 31/08/2016.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(ContactsActivity activity);
}
